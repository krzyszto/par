#include <stdio.h>
#include <algorithm>
#include <assert.h>

struct wezel {
	int lsyn;
	int psyn;
	int glebokosc;
};

const int nullwierzcholek = -1;

void usunDrzewo(wezel **drzewo, const int dlugoscGlowna)
{
	assert(drzewo != nullptr);
	assert(dlugoscGlowna > 0);
	for (int i = 0; i < dlugoscGlowna; i++) {
		delete drzewo[i];
	}
	delete[] drzewo;
	drzewo = nullptr;
}

void initDrzewo(wezel **drzewo, const int dlugoscGlowna)
{
	assert(drzewo == nullptr);
	assert(dlugoscGlowna > 0);
	drzewo = new wezel*[dlugoscGlowna];
	for (int i = 0; i < dlugoscGlowna; i++) {
		drzewo[i] = new wezel;
	}
}

void ustawWartosci(wezel **drzewo, const int dlugoscGlowna)
{
	assert(drzewo != nullptr);
	assert(dlugoscGlowna > 0);
	for (int i = 0; i < dlugoscGlowna; i++) {
		scanf("%i%i", &(drzewo[i]->lsyn), &(drzewo[i]->psyn));
	}
}

int main(int argc, char **argv)
{
	int liczbaWezlow;
	wezel **drzewo = nullptr;
	scanf("%i", &liczbaWezlow);
	initDrzewo(drzewo, liczbaWezlow);
	ustawWartosci(drzewo, liczbaWezlow);
	/* Skoczna Puma!*/
	/*	for*/
	printf("segfault\n");

	usunDrzewo(drzewo, liczbaWezlow);
	return 0;
}
